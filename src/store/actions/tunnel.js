export default {
  // 设置示例数据
  setInstance({ commit }, instanceArrayOfClassName) {
    commit("SET_INSTANCE", instanceArrayOfClassName);
  },
  // 修改数据
  // operateState(0:全部替换(默认), 1:增量替换)
  setParamValue({ commit }, valueOfParam, operateState = 0) {
    if (Array.isArray(valueOfParam)) {
      valueOfParam.forEach((item) => {
        commit("SET_Param_Value", item);
      });
    } else {
      commit("SET_Param_Value", Object.assign(valueOfParam, { operateState }));
    }
  },
  // 设置实例某个动作为执行状态
  setInstanceAction({ commit }, classNameUuIdActionNameFlag) {
    commit("SET_INSTANCE_ACTION", classNameUuIdActionNameFlag);
  },
  // 设置实例高亮
  setInstanceHighLight({ commit }, classNameUuIdReset) {
    commit("SET_INSTANCE_HighLight", classNameUuIdReset);
  },
};
