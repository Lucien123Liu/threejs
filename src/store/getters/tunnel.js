export default {
  getInstance(state) {
    return state.instances;
  },
  getMixers(state) {
    return state.mixers;
  },
  getGuiControl(state) {
    return {
      gui: state.gui,
      control: state.guiControl,
    };
  },
  noticeShow(state) {
    return state.noticeShow;
  },
};
