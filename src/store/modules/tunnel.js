import state from "../state/tunnel";
import mutations from "../mutations/tunnel";
import actions from "../actions/tunnel";
import getters from "../getters/tunnel";

export default {
  state,
  mutations,
  actions,
  getters,
};
