// 增量更新源数据
function updateObj({ param, value }, type = "object") {
  switch (type) {
    case "array":
      this[param] = [...new Set((this[param] || []).concat(value))];
      break;
    case "object":
    default:
      Object.assign(this, value);
      break;
  }
}

// 获取某个类(className)下面的某个实例(uuId)
function getInstance(state, params) {
  if (state.instances[params.className]) {
    return state.instances[params.className].find(
      (Fan) => Fan.uuId === params.uuId
    );
  }
}

export default {
  // 设置实例数据
  SET_INSTANCE: function (state, instanceArrayOfClassName) {
    updateObj.call(
      state.instances,
      {
        param: instanceArrayOfClassName.className,
        value: instanceArrayOfClassName.instances,
      },
      "array"
    );
    // state.instances[instanceArrayOfClassName.className] = instanceArrayOfClassName.instances
  },
  // 修改数据
  // valueOfParamAndOperateState.operateState(0:全部替换(默认), 1:增量替换)
  SET_Param_Value: function (state, valueOfParamAndOperateState) {
    if (!valueOfParamAndOperateState.operateState) {
      state[valueOfParamAndOperateState.param] =
        valueOfParamAndOperateState.value;
    } else {
      updateObj.call(state[valueOfParamAndOperateState.param], {
        param: valueOfParamAndOperateState.param,
        value: valueOfParamAndOperateState.value,
      });
    }
  },
  // 设置实例某个动作为执行状态
  SET_INSTANCE_ACTION(state, classNameUuIdActionNameFlag) {
    // 根据类名和实例id找到相应的实例
    const instance = getInstance(state, classNameUuIdActionNameFlag);
    instance &&
      instance.setRunAction({
        actionName: classNameUuIdActionNameFlag.actionName,
        flag: classNameUuIdActionNameFlag.flag,
      }); // 执行实例动画方法
  },
  // 设置实例某个属性状态
  SET_INSTANCE_HighLight(state, classNameUuIdReset) {
    // 根据类名和实例id找到相应的实例
    const instance = getInstance(state, classNameUuIdReset);
    instance && instance.setHighLight(classNameUuIdReset.reset); // 执行实例方法-高亮or正常显示
  },
};
