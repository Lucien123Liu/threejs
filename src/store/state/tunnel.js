import { GUI } from "dat.gui";
export default {
  instances: {}, // 所有实例合集object<className:[]instances>
  noticeShow: false, // 消息通知面板显示标志
  gui: new GUI(), // gui实例对象
  guiControl: {}, // 控制GUI显示面板字段集合
  mixers: [], // 除了实例外的所有animationMixer实例
  event: {}, // 鼠标点击事件对象
};
