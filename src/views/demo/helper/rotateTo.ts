import TWEEN from "@tweenjs/tween.js";
import { Box3, Intersection, Vector3 } from "three";

export default function focus(
  this: any,
  obj: Intersection | undefined,
  cb?: () => void
) {
  if (!obj || !this) {
    return false;
  }
  const _this = this;
  const box = new Box3().setFromObject(obj.object);
  const size = new Vector3();
  box.getSize(size);
  if (size.z > 300) {
    size.z = 0;
  }
  const curCameraP = _this.camera.position; // 当前相机位置
  const targetMeshP = {
    x: obj.point.x,
    y: obj.point.y,
    z: obj.point.z + (obj.point.z > 0 ? size.z + 100 : -size.z - 100),
  }; // 新相机的目标位置
  const target1 = _this.controls.target; // 相机当前controls的target焦点
  const target2 = obj.point; // 新的相机的controls的target焦点
  const positionVar = {
    x1: curCameraP.x,
    y1: curCameraP.y,
    z1: curCameraP.z,
    x2: target1.x,
    y2: target1.y,
    z2: target1.z,
  };
  // console.log(curCameraP, '当前相机位置')
  // console.log(targetMeshP, '新相机的目标位置')
  // console.log(target1, '相机当前controls的target焦点')
  // console.log(target2, '新的相机的controls的target焦点')
  _this.controls.enabled = false;
  new TWEEN.Tween(positionVar)
    .to(
      {
        x1: targetMeshP.x,
        y1: targetMeshP.y,
        z1: targetMeshP.z,
        x2: target2.x,
        y2: target2.y,
        z2: target2.z,
      },
      1000
    )
    .easing(TWEEN.Easing.Quadratic.Out)
    .onUpdate(() => {
      _this.camera.position.x = positionVar.x1;
      _this.camera.position.y = positionVar.y1;
      _this.camera.position.z = positionVar.z1;
      _this.controls.target.x = positionVar.x2;
      _this.controls.target.y = positionVar.y2;
      _this.controls.target.z = positionVar.z2;
      _this.controls.update();
      _this.camera.lookAt(target2);
      _this.camera.updateProjectionMatrix();
    })
    .onComplete(() => {
      _this.controls.enabled = true;
      cb && cb();
    })
    .start(); // 设置缓动动画
}
