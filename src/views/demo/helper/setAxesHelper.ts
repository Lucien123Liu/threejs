import { AxesHelper, Scene } from "three";
import Template from "../Class/Template";

export default function setAxesHelper(wrap3d: Template) {
  const axesH = new AxesHelper(500);
  wrap3d.scene.add(axesH);
}
