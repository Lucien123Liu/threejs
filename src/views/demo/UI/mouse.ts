import {
  Vector2,
  Color,
  Raycaster,
  Intersection,
  Object3D,
  Camera,
  Scene,
  WebGLRenderer,
} from "three";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer.js";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass.js";
import { OutlinePass } from "three/examples/jsm/postprocessing/OutlinePass.js";
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass.js";
import { FXAAShader } from "three/examples/jsm/shaders/FXAAShader.js";
import store from "@/store";
import { Dispatcher } from "./actions";
import Template from "../Class/Template";

const mouse: {
  x: number;
  y: number;
} = { x: 0, y: 0 };
let intersects: Intersection[], target: Intersection | undefined;
function getIntersects(event: MouseEvent, wrap3d: Template) {
  mouse.x =
    ((event.clientX - wrap3d.el.offsetLeft) / wrap3d.el.clientWidth) * 2 - 1; // dom.offsetLeft -- dom元素距离浏览器左侧的距离   dom.clientWidth -- dom元素宽度
  mouse.y =
    -((event.clientY - wrap3d.el.offsetTop) / wrap3d.el.clientHeight) * 2 + 1; // dom.offsetTop -- dom元素距离浏览器顶部的距离    dom.clientHeight -- dom元素高度
  // let vector = new Vector3(mouse.x, mouse.y, 0.5)
  // vector = vector.unproject(wrap3d.camera)
  // const raycaster = new Raycaster(wrap3d.camera.position, vector.sub(wrap3d.camera.position).normalize())
  const vector2 = new Vector2(mouse.x, mouse.y);
  const raycaster = new Raycaster();
  raycaster.setFromCamera(vector2, wrap3d.camera);

  intersects = raycaster.intersectObjects(wrap3d.scene.children, true);
}

export function onDocumentMouseDown(event: PointerEvent, wrap3d: Template) {
  if (event.button !== 0) {
    // 仅执行鼠标左键点击
    return;
  }
  console.log(event, wrap3d.el)
  console.log(((event.clientX - wrap3d.el.offsetLeft) / wrap3d.el.clientWidth) * 2 - 1)
  getIntersects(event, wrap3d);
  intersects = intersects.filter((item) => item.object.userData.UI);
  if (intersects.length > 0) {
    target = intersects[0];
    const actions = target.object.userData.actions || [];
    actions.forEach((actionName: any) => {
      Dispatcher(actionName, wrap3d, target);
    });
    // outlineObj([target.object], wrap3d) // 高亮
    store.dispatch("setParamValue", [
      {
        param: "noticeShow",
        value: true,
      },
      {
        param: "event",
        value: event,
      },
    ]);
  } else {
    // wrap3d.composer = null
    store.dispatch("setParamValue", {
      param: "noticeShow",
      value: false,
    });
  }
}

export function onDocumentMouseMove(event: MouseEvent, wrap3d: Template) {
  getIntersects(event, wrap3d);
  intersects = intersects.filter((item) => item.object.userData.UI);
  if (intersects.length > 0) {
    target = intersects[0];
    target &&
      store.dispatch("setInstanceHighLight", {
        className: target.object.userData.className,
        uuId: target.object.userData.uuId,
        reset: false,
      });
    document.body.style.cursor = "pointer";
  } else {
    target &&
      store.dispatch("setInstanceHighLight", {
        className: target.object.userData.className,
        uuId: target.object.userData.uuId,
        reset: true,
      });
    target = undefined;
    document.body.style.cursor = "auto";
  }
}

export function onWindowResize(
  wrap3d: {
    el: { offsetWidth: any; offsetHeight: any };
    rendererWidth: number;
    rendererHeight: number;
    camera: { aspect: number; updateProjectionMatrix: () => void };
    renderer: { setSize: (arg0: any, arg1: any) => void };
  }
) {
  const { offsetWidth, offsetHeight } = wrap3d.el;
  wrap3d.rendererWidth = offsetWidth;
  wrap3d.rendererHeight = offsetHeight;
  if (wrap3d.camera) {
    wrap3d.camera.aspect = wrap3d.rendererWidth / wrap3d.rendererHeight;
    wrap3d.camera.updateProjectionMatrix();
  }
  wrap3d.renderer.setSize(wrap3d.rendererWidth, wrap3d.rendererHeight);
}

// eslint-disable-next-line no-unused-vars
function outlineObj(
  selectedObjects: Object3D[],
  wrap3d: {
    composer: EffectComposer;
    renderer: WebGLRenderer;
    scene: Scene;
    camera: Camera;
    el: { clientWidth: number | undefined; clientHeight: number | undefined };
  }
) {
  // 创建一个EffectComposer（效果组合器）对象，然后在该对象上添加后期处理通道。
  wrap3d.composer = new EffectComposer(wrap3d.renderer);
  // 新建一个场景通道  为了覆盖到原理来的场景上
  const renderPass = new RenderPass(wrap3d.scene, wrap3d.camera);
  wrap3d.composer.addPass(renderPass);
  // 物体边缘发光通道
  const outlinePass = new OutlinePass(
    new Vector2(wrap3d.el.clientWidth, wrap3d.el.clientHeight),
    wrap3d.scene,
    wrap3d.camera,
    selectedObjects
  );
  outlinePass.selectedObjects = selectedObjects;
  outlinePass.edgeStrength = 10.0; // 边框的亮度
  outlinePass.edgeGlow = 1; // 光晕[0,1]
  outlinePass.usePatternTexture = false; // 是否使用父级的材质
  outlinePass.edgeThickness = 1.0; // 边框宽度
  outlinePass.downSampleRatio = 1; // 边框弯曲度
  outlinePass.pulsePeriod = 5; // 呼吸闪烁的速度
  outlinePass.visibleEdgeColor.setHex(0x00ff00); // 呼吸显示的颜色
  outlinePass.hiddenEdgeColor = new Color(1, 1, 1); // 呼吸消失的颜色
  outlinePass.clear = true;
  wrap3d.composer.addPass(outlinePass);
  // 自定义的着色器通道 作为参数
  const effectFXAA = new ShaderPass(FXAAShader); // 处理锯齿
  effectFXAA.uniforms.resolution.value.set(
    1 / (wrap3d.el.clientWidth || 1),
    1 / (wrap3d.el.clientHeight || 1)
  );
  effectFXAA.renderToScreen = true;
  wrap3d.composer.addPass(effectFXAA);
}
