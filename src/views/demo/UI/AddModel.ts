/* eslint-disable no-unused-vars */
import { AnimationMixer, Scene } from "three";
import GLTFLoadFan from "../Objects/GLTFLoadFan";
import FishFbx from "../Objects/FishFbx";
import store from "@/store";

let aimScene: Scene;
export function addModelToScene(scene: Scene) {
  aimScene = scene;
  addGLTFFan();
}

async function addGLTFFan() {
  try {
    store.dispatch("setParamValue", [
      {
        param: "guiControl",
        value: {
          风机1: false,
          风机2: false,
          风机3: false,
        },
        operateState: 1,
      },
    ]);
    const guiControl = store.getters.getGuiControl;
    const fans = guiControl.gui.addFolder("风机组");
    const instances: GLTFLoadFan[] = [
      new GLTFLoadFan(),
      new GLTFLoadFan(),
      new GLTFLoadFan(),
    ];
    for (let index = 0; index < instances.length; index++) {
      const classInstance = instances[index];
      await classInstance.loadData();
      classInstance.setPosition(10 * index, 10 * index, 10 * index);
      classInstance.addToScene(aimScene);
      classInstance.mixer = new AnimationMixer(classInstance.instance);
      const animation = classInstance.mixer
        .clipAction(classInstance.instance.animations[0])
        .stop();
      fans
        .add(guiControl.control, "风机" + (index + 1))
        .onChange(function (state: boolean) {
          animation[state ? "play" : "stop"]();
          // store.dispatch('setInstanceAction', {
          //   className: classInstance.instance.userData.className,
          //   uuId: classInstance.instance.userData.uuId,
          //   actionName: 'turnTurn',
          //   flag: state
          // })
        });
    }
    fans.open();
    const valueByKey = {
      className: "GLTFLoadFan",
      instances: instances,
    };
    store.dispatch("setInstance", valueByKey);
  } catch (error) {
    console.log(error);
  }
}

// fbx动画鱼
async function addFishFbx() {
  try {
    const instances = [new FishFbx(), new FishFbx(), new FishFbx()];
    const valueByKey = {
      className: "GLTFLoadFan",
      instances: instances,
    };
    for (let index = 0; index < instances.length; index++) {
      const classInstance = await instances[index].loadData();
      classInstance.setPosition(10 * index, 10 * index, 10 * index);
      classInstance.mixer = new AnimationMixer(classInstance.instance);
      classInstance.mixer
        .clipAction(classInstance.instance.animations[0])
        .play();
      classInstance.addToScene(aimScene);
    }
    store.dispatch("setInstance", valueByKey);
  } catch (error) {
    console.log(error);
  }
}

function onProgress(xhr: ProgressEvent) {
  if (xhr.lengthComputable) {
    const percentComplete = (xhr.loaded / xhr.total) * 100;
    console.log(Math.round(percentComplete) + "% downloaded");
  }
}
function onError(e: ErrorEvent) {
  throw e;
}
