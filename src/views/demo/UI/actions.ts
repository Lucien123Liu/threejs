import focus from "../helper/rotateTo";
import store from "@/store";
import { AnimationMixer, Clock, Intersection } from "three";
import Template from "../Class/Template";

const clock = new Clock();
const instances = store.getters.getInstance;

export function Dispatcher(
  action: string | (() => void),
  wrap3d: Template,
  target: Intersection | undefined
) {
  if (!target) {
    return;
  }
  switch (action) {
    case "focus":
      focus.call(wrap3d, target);
      break;
    case "turnTurn":
      store.dispatch("setInstanceAction", {
        className: target?.object.userData.className,
        uuId: target?.object.userData.uuId,
        actionName: action,
        flag: true,
      });
      break;
    default:
      typeof action === "function" && action();
      break;
  }
}
// 查找所有实例可执行的方法，执行
export function allInstanceMove() {
  Object.keys(instances).forEach((nameOfClass) => {
    instances[nameOfClass].forEach((classInstance: { [key: string]: any }) => {
      Object.keys(classInstance.runAction)
        .filter((actionName) => classInstance.runAction[actionName])
        .forEach((actionName: string) => {
          classInstance[actionName]({ rad: 0.25 });
        });
    });
  });
}

// 所有mixer动画更新
export function updateMixer() {
  const delta = clock.getDelta();
  const mixers = store.getters.getMixers || [];
  mixers.forEach((mixer: AnimationMixer) => mixer.update(delta));
  Object.keys(instances).forEach((className) => {
    instances[className].forEach((instance: { mixer: AnimationMixer }) => {
      if (instance.mixer) {
        instance.mixer.update(delta);
      }
    });
  });
}
