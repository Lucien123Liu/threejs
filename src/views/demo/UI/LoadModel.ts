import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";
const gltfLoader = new GLTFLoader();
// 这个是Threejs解析draco压缩之后的解析器,它从这里读取解析器JS
const dracoLoader = new DRACOLoader();

interface loadParams {
  path: string;
  modelName: string;
  onProgress?: () => void;
  onError?: () => void;
}

export function loadGLTF({
  path,
  modelName,
  onProgress,
  onError,
}: loadParams): Promise<GLTF> {
  gltfLoader.setPath(path);
  return new Promise((resolve, reject) => {
    dracoLoader.setDecoderPath("/draco/");
    gltfLoader.setDRACOLoader(dracoLoader);
    gltfLoader.load(
      modelName,
      function (gltf) {
        resolve(gltf);
      },
      onProgress,
      (e) => {
        onError && onError();
        reject(e);
      }
    );
  });
}
