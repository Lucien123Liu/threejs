import { AmbientLight, DirectionalLight, Scene } from "three";

export function addAmbientLight(scene: Scene) {
  const ambientColor = new AmbientLight();
  scene.add(ambientColor);
}

export function addDirectionalLight(scene: Scene) {
  const light = new DirectionalLight();
  // light.castShadow = true // default false
  light.position.set(50, 200, 100);
  scene.add(light);
}
