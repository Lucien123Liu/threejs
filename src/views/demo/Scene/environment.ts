import { UnsignedByteType, PMREMGenerator } from "three";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
import Template from "../Class/Template";

export function addEnvironment(wrap3d: Template) {
  const pmremGenerator = new PMREMGenerator(wrap3d.renderer);
  pmremGenerator.compileEquirectangularShader();
  const str = "/static/images/immenstadter_horn_1k.hdr";
  new RGBELoader().setDataType(UnsignedByteType).load(str, function (texture) {
    const envMap = pmremGenerator.fromEquirectangular(texture).texture;
    wrap3d.scene.environment = envMap;
    // wrap3d.scene.background = envMap // 给场景添加背景图
    // texture.dispose()
    pmremGenerator.dispose();
  });
}
