import { Color, PerspectiveCamera, Scene, Vector3, WebGLRenderer } from "three";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";

interface web3D {
  scene: Scene;
  renderer: WebGLRenderer;
  camera: PerspectiveCamera;
  el: HTMLElement;
}

export default class Template implements web3D {
  el: HTMLElement;
  scene: Scene;
  renderer: WebGLRenderer = new WebGLRenderer();
  camera: PerspectiveCamera = new PerspectiveCamera();
  clearColor: Color = new Color(0xffffff);
  rendererWidth: number;
  rendererHeight: number;
  cameraPosition: Vector3 = new Vector3(0, 0, 1);
  cameraLookAt: number | Vector3 = new Vector3(0, 0, 0);
  alpha = 1.0;
  controls: any;
  composer: EffectComposer | undefined = undefined;
  constructor(domId: string) {
    this.el = document.getElementById(domId) || document.body;
    this.scene = new Scene();
    this.rendererWidth = window.innerWidth;
    this.rendererHeight = window.innerHeight;
    this.camera.aspect = this.rendererWidth / this.rendererHeight;
  }

  private initPerspectiveCamera() {
    const cameraOptions = {
      fov: 45,
      aspect: 1,
      near: 1,
      far: 5000,
    };
    if (this.camera instanceof PerspectiveCamera) {
      this.scene.remove(this.camera);
    }
    const camera = new PerspectiveCamera(
      cameraOptions.fov,
      cameraOptions.aspect,
      cameraOptions.near,
      cameraOptions.far
    );
    camera.position.copy(this.cameraPosition);
    camera.lookAt(this.cameraLookAt);
    this.camera = camera;
    this.scene.add(camera);
  }

  private initScene() {
    this.scene = new Scene();
  }

  initCamera() {
    this.initPerspectiveCamera();
  }

  initRender() {
    const renderer = new WebGLRenderer({
      antialias: true,
      alpha: this.alpha !== 1.0,
    });
    renderer.render(this.scene, this.camera);
    renderer.setClearColor(this.clearColor);
    renderer.setSize(window.innerWidth, window.innerHeight);
    this.el.appendChild(renderer.domElement);
    this.renderer = renderer;
  }

  init() {
    this.initScene();
    this.initCamera();
    this.initRender();
  }
}
