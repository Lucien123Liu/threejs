import {
  AnimationMixer,
  Box3,
  Color,
  Mesh,
  Object3D,
  Scene,
  Vector3,
} from "three";
import { generateChar } from "@/common/js/util";

interface actionNameFlag {
  actionName: string;
  flag: boolean;
}

interface runAction {
  [propName: string]: boolean;
}

export default class Base {
  normalColors: Color[] | number[] = []; // 实例子元素的材质颜色信息（只保留了一层children的数据）
  instance: Object3D = new Object3D(); // object实例
  runAction: runAction = {}; // 存储需要执行的动画方法名
  uuId: string;
  onError?: () => void; // 加载失败回调
  onProgress?: () => void; // 加载回调
  highColor = 0xffffff;
  mixer: AnimationMixer | null = null;
  // 加载回调
  constructor() {
    this.uuId = generateChar(5); // 设置实例默认uuId
  }

  setPosition(x: number, y: number, z: number) {
    this.instance.position.set(x, y, z);
  }

  // 设置实例的方法执行状态（true/false）
  setRunAction(actionNameFlag: actionNameFlag) {
    this.runAction[actionNameFlag.actionName] = actionNameFlag.flag || false;
  }

  setRotation(x: number, y: number, z: number) {
    this.instance.rotation.x = x;
    this.instance.rotation.y = y;
    this.instance.rotation.z = z;
  }

  // 设置高亮reset(回复本来颜色) 默认循环一次children
  setHighLight(reset: boolean) {
    switch (this.instance.type) {
      case "Group":
        this.instance.children.forEach((child: Object3D, index: number) => {
          if (child instanceof Mesh) {
            child.material.color.set(
              (reset ? this.normalColors[index] : this.highColor) || 0xffffff
            );
          }
        });
        break;
      default:
        break;
    }
  }

  addToScene(scene: Scene, Cb?: () => void) {
    scene.add(this.instance);
    if (Cb && Array.isArray(Cb)) {
      Cb.forEach((fun) => {
        fun();
      });
    } else {
      Cb && Cb();
    }
  }

  getSize(instance: Object3D) {
    const box = new Box3();
    const sizeV3 = new Vector3();
    return box.setFromObject(instance || this.instance).getSize(sizeV3);
  }

  rotateByAxis(axis = "x", theta: number) {
    switch (axis.toLocaleLowerCase()) {
      case "x":
        this.instance.rotation.x = theta;
        break;
      case "y":
        this.instance.rotation.y = theta;
        break;
      case "z":
        this.instance.rotation.z = theta;
        break;
      default:
        break;
    }
  }
}
