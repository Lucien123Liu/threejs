import Base from "../Class/BaseModel";
import { AnimationClip, Group, Matrix4, Mesh, Object3D } from "three";
import { loadGLTF } from "../UI/LoadModel";
import { SkeletonUtils } from "three/examples/jsm/utils/SkeletonUtils";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";

let gltfData: Group;
let animations: AnimationClip[];
async function getGltf(): Promise<Object3D> {
  try {
    const gltf: GLTF = await loadGLTF({
      path: "/static/assets/fan/",
      modelName: "风机动画.gltf",
    });
    console.log(gltf);
    gltf.scene.traverse((mesh: Object3D) => {
      mesh.position.set(0, 0, 0);
      if (mesh instanceof Mesh) {
        mesh.geometry && mesh.geometry.computeBoundingBox();
        mesh.geometry && mesh.geometry.center();
      }
    });
    gltfData = gltf.scene;
    animations = gltf.animations || [];
    gltf.scene.userData.className = "GLTFLoadFan";
    return gltf.scene;
  } catch (error) {
    console.log(error);
    return new Object3D();
  }
}
interface ModelParam {
  onProgress?: () => void;
  onError?: () => void;
}

interface ObjAny {
  [propName: string]: any;
}

export default class GLTFLoadFan extends Base {
  highColor = 0xff0000;
  animations: AnimationClip[] = [];
  constructor(params?: ModelParam) {
    super();
  }

  async loadData() {
    if (gltfData) {
      this.instance = await SkeletonUtils.clone(gltfData);
    } else {
      this.instance = await getGltf();
    }
    this.instance.animations = animations;
    this.instance.userData.uuId = this.uuId;
    return this;
  }

  turnTurn({ rad }: ObjAny) {
    const mt4 = new Matrix4();
    mt4.makeRotationZ(rad || 0.05);
    this.instance.getObjectByName("ceiling_fan")?.applyMatrix4(mt4);
  }
}
