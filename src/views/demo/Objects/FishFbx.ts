import Base from "../Class/BaseModel";
// import {
//   AnimationMixer,
// } from 'three'
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
import { SkeletonUtils } from "three/examples/jsm/utils/SkeletonUtils";
import { AnimationClip, Group } from "three";

let fbxData: Group;
let animations: AnimationClip[];
async function getFbx(): Promise<Group> {
  const fbxloader = new FBXLoader();
  try {
    return new Promise((resolve, reject) => {
      fbxloader.load(
        "/static/assets/fish/bullfish_03_final_v02_.fbx",
        (object: Group) => {
          console.log(object);
          object.scale.set(0.5, 0.5, 0.5);
          object.userData.className = "FishFbx";
          object.userData.UI = true;
          fbxData = object;
          animations = object.animations || [];
          resolve(fbxData);
        },
        undefined,
        reject
      );
    });
  } catch (error) {
    console.log(error);
    return new Group();
  }
}

export default class FishFbx extends Base {
  highColor = 0xff0000;
  constructor() {
    super();
  }

  async loadData() {
    if (fbxData) {
      this.instance = await SkeletonUtils.clone(fbxData);
    } else {
      this.instance = await getFbx();
    }
    this.instance.animations = animations || [];
    this.instance.userData.uuId = this.uuId;
    return this;
  }
}
