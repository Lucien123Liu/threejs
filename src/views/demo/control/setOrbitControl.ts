import { Camera } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

export function setControl(camera: Camera, el: HTMLElement) {
  const controls = new OrbitControls(camera, el);
  // 初始化控制器
  // 设置相机缩放的中心点
  // controls.target.set(0, 0, 0)
  controls.maxPolarAngle = Math.PI * 0.45;
  // 如果使用animate方法时，将此函数删除
  // controls.addEventListener('change', () => {
  //   console.log(camera.getEffectiveFOV())
  // })
  // 使动画循环使用时阻尼或自转 意思是否有惯性
  // controls.enableDamping = true
  // 动态阻尼系数 就是鼠标拖拽旋转灵敏度
  // controls.dampingFactor = 0.25;
  // 是否可以缩放
  // controls.enableZoom = true
  // 是否自动旋转
  // controls.autoRotate = false
  // 设置相机距离原点的最远距离
  // controls.minDistance = 20
  // 设置相机距离原点的最远距离
  // controls.maxDistance = 10000
  // 是否开启右键拖拽 默认是
  controls.enablePan = true;
  return controls;
}
