export function generateChar(_num = 3): string {
  return Math.random().toString(16).substr(2, _num);
}

/**
 * debounce去抖动
 * @author   lvzhiyuan
 * @date     2018/9/20
 * @param    method——需要防抖的函数
 * @param    delay——延迟delay秒后才执行method
 */
export function debounce(method: any, delay = 200): () => void {
  let timer: any;
  return function (this: any, ...args: any[]) {
    const context = this;
    clearTimeout(timer);
    timer = setTimeout(function () {
      method(context, args);
    }, delay);
  };
}

/**
 *@functionName: throttle
 *@params1: method需要节流的方法名, wait多少毫秒执行一次
 *@description: 节流函数
 *@author: liuzhaojun
 *@date: 2021/07/12 10:30:12
 *@version: V1.0.0
 */
export function throttle(method: any, wait: number): () => void {
  let pre = Date.now();
  return function (this: any, ...args: any[]) {
    const context = this;
    const now = Date.now();
    if (now - pre >= wait) {
      method.apply(context, args);
      pre = Date.now();
    }
  };
}
